import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PokemonImageComponent } from './pokemon-image/pokemon-image.component';
import { PokemonComponent } from './pokemon/pokemon.component';
import { LoadedDirective } from './loaded-directive.directive';

@NgModule({
  declarations: [
    AppComponent,
    PokemonImageComponent,
    PokemonComponent,
    LoadedDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
