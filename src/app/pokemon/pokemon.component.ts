import { Component, OnInit } from '@angular/core';
import { POKEMON_NUMS } from '../const';
import { PokemonService } from '../pokemon.service';
import { PokemonDto } from '../pokemon-dto';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {

  pokemons: PokemonDto[] = [];
  showPokemonCard: boolean = false;
  imgFullyLoaded: boolean = false;
  counter: number = 0;
  order = [ 0, 1, 2, 3 ];
  
  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.generatePokemon();
  }

  async generatePokemon() {
    for(let idx = 0; idx < 4; idx++) {
      let randomID = Math.floor(Math.random() * POKEMON_NUMS + 1);
      
      await this.pokemonService.getPokemon(randomID).then(pokemon => {
        this.pokemons[idx] = new PokemonDto();
        this.pokemons[idx].name = this.toTitleCase(pokemon.name);
        this.pokemons[idx].sprite = pokemon.sprites.other.home.front_default;
      });
    }

    this.shuffle(this.order);

    this.showPokemonCard = true;
  }
  
  isLoaded() {
    this.imgFullyLoaded = true;
  }

  getAnswer(name: string, id: string) {
    if(name === this.pokemons[this.order[0]].name) {
      this.counter++;
      document.getElementById("btn-" + id).classList.add('guessed');
    } else {
      document.getElementById("btn-" + id).classList.add('wrong');
      for (let i = 0; i < 4; i++)
        if(document.getElementById("btn-" + i).innerHTML == this.pokemons[this.order[0]].name) 
          document.getElementById("btn-" + i).classList.add('guessed');
    }

    setTimeout(() => {
      this.generatePokemon();
    }, 1500);
  }

  // https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
  toTitleCase(str) {
    return str.replace(
      /\w\S*/g,
      function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  }

  // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
  shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

}
