let baseUrl = 'https://pokeapi.co/api/v2';

export const REQ = {
    GET_POKEMON: `${baseUrl}/pokemon/`
}

export const POKEMON_NUMS = 300;