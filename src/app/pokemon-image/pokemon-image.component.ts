import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon-image',
  templateUrl: './pokemon-image.component.html',
  styleUrls: ['./pokemon-image.component.scss']
})
export class PokemonImageComponent implements OnInit {

  @Input() src: string;
  @Input() alt: string;
  @Output() loaded = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  isLoaded() {
    this.loaded.emit();
  }

}
