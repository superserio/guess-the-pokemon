import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { REQ } from './const';
import { PokemonDto } from './pokemon-dto';

@Injectable()
export class PokemonService {

  constructor(private http: HttpClient) { }

  getPokemon = (_id: number) => this.http.get<any>(REQ.GET_POKEMON + _id).toPromise(); 

}
